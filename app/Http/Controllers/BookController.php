<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\BookValidationRequest;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$books = Book::all();
        
       $id = Auth::id();
       if (Gate::denies('admin')) {
           $boss = DB::table('normal')->where('normal',$id)->first();
          $id = $boss->admin;
       }
       $user = User::find($id);
       $books = $user->books;

        return view('books.index',['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('admin')) {
            abort(403,"Sorry you are not allowed to create books..");
        
        } 
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BookValidationRequest $request)
    {   
        if (Gate::denies('admin')) {
                        abort(403,"Are you a hacker or what?");
                   
                    }           
        $validated = $request->validated();    
        $book = new book();
        $id = Auth::id();
        $book->title = $request->title;
        $book->author = $request->author;   
        $book->user_id = $id;
        $book->status=0;
        $book->save();
        return redirect('books');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('admin')) {
                        abort(403,"Are you a hacker or what?");
                   
                    }
        $book = Book::find($id);
      return view('books.edit',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            //only if this todo belongs to user
            $book = Book::findOrFail($id);
            //employees are not allowed to change the title 
             if (Gate::denies('admin')) {
                 if ($request->has('update'))
                        abort(403,"You are not allowed to edit books..");
             }   
             if(Gate::allows('admin')){
             //make sure the todo belongs to the logged in user
             if(!$book->user->id == Auth::id()) return(redirect('books'));
             $book->update($request->except(['_token']));
              if($request->ajax()){
                   return Response::json(array('result' => 'success1','status' => $request->status ), 200);                      
                 }
             }
             if(Gate::allows('normal')){
                if($book->status == 0){
                if(!$book->user->id == Auth::id()) return(redirect('books'));
                  $book->update($request->except(['_token']));
                    if($request->ajax()){
                         return Response::json(array('result' => 'success1','status' => $request->status ), 200);                      
                       }
                  }
                }
             return redirect('books');
            
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('admin')) {
            abort(403,"Are you a hacker or what?");
                   
                    }


        $book = Book::find($id);
        $book ->delete();
        return redirect('books');
    }
}
