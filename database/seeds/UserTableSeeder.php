<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert
        (
            [   
                [
                    
                    'name'=>'ayelet siani',
                    'email'=>'msd@nbvd.co.il',
                    'password' => '1234',
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'normal',
                ],
                [
                    
                    'name'=>'ayelet',
                    'email'=>'msd@nbd.co.il',
                    'password' => '124',
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'normal',
                ],
                [
                    
                    'name'=>'siani',
                    'email'=>'msd@d.co.il',
                    'password' => '123',
                    'created_at' => date('Y-m-d G:i:s'),
                    'role' => 'normal',
                ]

            ] 
                );
    }
}
