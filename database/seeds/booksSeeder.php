<?php

use Illuminate\Database\Seeder;

class booksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
                [ 
                    'title' => 'BECOMING',
                    'author' => 'OBAMA, MICHELLE',
                    'created_at' => date('Y-m-d G:i:s'),
                    'user_id' => 1,
                ],
                [
                    'title' => 'SAPIENS',
                    'author' => 'HARARI, YUVAL NOAH',
                    'created_at' => date('Y-m-d G:i:s'),  
                    'user_id' => 2,
                ],
                [
                    'title' => 'STILL ME',
                    'author' => 'MOYES, JOJO',
                    'created_at' => date('Y-m-d G:i:s'),  
                    'user_id' => 3,
                ],
                [
                    'title' => 'ROSIE RESULT',
                    'author' => 'SIMSION, GRAEME',
                    'created_at' => date('Y-m-d G:i:s'),  
                    'user_id' => 4,
                ],
                [
                    'title' => 'WAVE N/E',
                    'author' => 'RHUE, MORTON',
                    'created_at' => date('Y-m-d G:i:s'),
                    'user_id' => 5,  
                ]
        ]);
    }
}
