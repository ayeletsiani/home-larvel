@extends('layouts.app')

@section('content')
<h1> Create a new reading book</h1>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method = 'post' action = "{{action('BookController@store')}}">
{{csrf_field()}} 
<div class="form-group">
    <label for="title"> Which book would you like to read? </label>
    <input type = "text" class="form=control" name="title">
</div>

<div class="form-group">
    <label for="author"> Author </label>
    <input type = "text" class="form=control" name="author">
</div>

<div class="form-group">
    <input type="submit" class="form=control" name="submit" value="Save">
</div>
</form>

@endsection