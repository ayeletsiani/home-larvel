@extends('layouts.app')

@section('content')
<h1> Edit book</h1>
<form method = 'post' action = "{{action('BookController@update', $book->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
        <label for="title"> Update the book name </label>
        <input type = "text" class="form=control" name="title" value="{{$book->title}}">
    </div>
    
    <div class="form-group">
        <label for="author"> Update the book author </label>
        <input type = "text" class="form=control" name="author" value="{{$book->author}}">
    </div>

    <div class="form-group">
        <input type="submit" class="form=control" name="submit" value="Update">
    </div>

</form>

<form method = 'post' action = "{{action('BookController@destroy', $book->id)}}">
    @csrf
    @method('DELETE')
  <div class="form-group">
        <input type="submit" class="form=control" name="submit" value="Delete">
    </div>

</form>

@endsection